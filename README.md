# Portable University Model of the Atmosphere (PUMA)

The PUMA model is a simplified, dry atmospheric model that has been developed at the University of Hamburg based on the spectral model of Hoskins and Simmons (1975). The source code is freely available and can be downloaded from https://www.mi.uni-hamburg.de/en/arbeitsgruppen/theoretische-meteorologie/modelle/puma.html

The model code is thoroughly commented and pretty readable, and nicely documented in the User's Guide (also available through the website above). However, I encountered some difficulties when I started to work with the model as a student - I hope that the hints and scripts collected here can help to get an easier start with PUMA!

## Compiling the model for parallel program execution

The general procedures for model compilation and running are described in the User's Guide, please read the instructions there. When the model shall be run on a single CPU, this works fine.

The PUMA code is fully parallelised, so running the model on multiple CPUs is possible. In Version 16 and 17 of the model code, an old version of the mpi_scatter and mpi_gather commands was used, which can let the compilation fail. If this has not been solved in newer code versions, this is what to do:

* In the files *puma/src/mpimod.f90* and *puma/src/mpimod_multi.f90* search all instances of **mpi_scatter** and **mpi_gather**. If the first and the fourth argument are equal, you have to do the following changes:

    * for **mpi_gather**: change the first argument to MPI_IN_PLACE (but only for the root process...), for example:

    Old code version:
    ```fortran
    do jlev = 1 , NLEV
             call mpi_gather(pcs(:,jlev),NLPP,mpi_rtype                      &
         &                  ,pcs(:,jlev),NLPP,mpi_rtype                      &
         &                  ,NROOT,myworld,mpinfo)

    enddo
    ```
    New code version:
    ```fortran
    do jlev = 1 , NLEV
             if (mypid == NROOT) then
                 call mpi_gather(MPI_IN_PLACE,NLPP,mpi_rtype                &
         &                      ,pcs(:,jlev),NLPP,mpi_rtype                 &
         &                      ,NROOT,myworld,mpinfo)
             else
                 call mpi_gather(pcs(:,jlev),NLPP,mpi_rtype &
         &                      ,pcs(:,jlev),NLPP,mpi_rtype &
         &                      ,NROOT,myworld,mpinfo)
             end if
    enddo
    ```

    * for **mpi_scatter**: change the fourth argument to MPI_IN_PLACE for the root process and the first argument for all other processes, for example: 

    Old code version:
    ```fortran
    call mpi_scatter(p,n,MPI_REAL8,p,n,MPI_REAL8,NROOT,myworld,mpinfo)
    ```
    New code version:
    ```fortran
    if (mypid == NROOT) then
        call mpi_scatter(p,n,MPI_REAL8,MPI_IN_PLACE,n,MPI_REAL8,NROOT,myworld,mpinfo)
    else
        call mpi_scatter(MPI_IN_PLACE,n,MPI_REAL8,p,n,MPI_REAL8,NROOT,myworld,mpinfo)
    end if
    ```

* Now compile the model as described in the User's Guide

## Defining your own input files

If you want to define your own orography and/or temperature only at the surface, this is possible with the PUMA preprocessor that is part of the PUMA code package. You can do this with the graphical user interface or by manipulating the preprocessor namelist. However, if you want to control the temperature field or damping parameters at all heights, you have to generate the PUMA input files yourself. Some example scripts for input file generation can be found in the **./input_files_gen** folder in this repository. 

## Note:

Be aware that some namelist variables have apparently changed names in the code, but not in the User's guide! This might have been fixed in newer versions of the User's Guide though. 

## References

Hoskins, B. and A. Simmons, 1975: A multi-layer spectral model and the semi-implicit method. _Quarterly Journal of the Royal Meteorological Society_, **101**, 637-655.