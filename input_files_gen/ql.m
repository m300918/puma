function [q] = ql(k,p)
% function ql from gaussmod.f90 for Legendre-Gauss grid definition
z0=acos(p);
z1=1;
z2=0;
for i=k:-2:0
    z3=z1*cos(z0*i);
    z2=z2+z3;
    z4=(k-i+1)*(k+i)*0.5;
    z1=z1*z4/(z4+(i-1));
end
if mod(k,2) == 0
    z2=z2-0.5*z3;
end
z0=sqrt(2);
for i=1:k
    z0=z0*sqrt(1-0.25/i^2);
end
q=z0*z2;
