function [q] = qld(k,p)
% function qld from gaussmod.f90 for Legendre-Gauss grid definition
z=p*ql(k,p)-sqrt((k+k+1)/(k+k-1))*ql(k-1,p);
q=(p^2-1)/(k*z);