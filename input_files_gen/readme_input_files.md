# Generate PUMA input files

PUMA takes five different input files that have to be named N064_surf_0???.sra (the *064* is the number of latitudes - this has to be changed depending on the horizontal resolution). The ??? are:

* **121** for the constant part of the restoration temperature
* **122** for the variable part of the restoration temperature (e.g. for a seasonal cycle)
* **123** for the damping coefficients
* **129** for the orography
* **134** for the surface pressure

The first three fields have to be given in three dimensions, the last two only at the surface. 

The supplied example script **generate_puma_input_files.m** generates these five fields for a horizontal resolution of T42 (64 x 128) and 30 vertical layers. (The script is written in Matlab, but it should be relatively easy to transfer the code to e.g. Python if Matlab is not available. Most of the code is a reproduction of the PUMA preprocessor source code - it is of course also possible to just implement the changes in the original Fortran code.) The functions **ql.m** and **qld.m** are needed for the Gauss-Legendre transformation in the script.