clc
close all
clear

%% Script to create initialisation files for PUMA (based on the PUMA preprocessor code)

% This example script generates a zonally symmetric temperature distribution with one winter hemisphere that has a sinusoidal shape in the meridional direction,
% and a Gaussian-shaped mountain range. The fields can be modified as needed.
% The temperature distribution and damping parameters are based on Held and Suarez (1994).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The files have to be named N***_surf_0???.sra with *** as the number of latitudes used (e.g. 064 for a 64x128 grid) and ??? as one of the following:
%
%   121 constant part of the restoration temperature        - all levels
%   122 variable part of the restoration temperature (e.g. winter-summer pole difference with seasonal cycle)     - all levels
%   123 reciprocal of damping time scales [1/s]             - all levels
%   129 topography [m2/s2] (as geopotential height...)      - 1 level
%   134 surface pressure [hPa]                              - 1 level
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Define parameters (see PUMA code for explanations)
nlon=128;
nlat=64;
nlev=30;
tgr=288.15;
ALR=0.0065;
dtrop=11000;
GA=9.81;
GASCON=287.0;
nlevt=9;
sigmax=6e-7;
dtns=-20;  % winter to summer pole temperature difference
dtep=60;   % equator to pole temperature difference
PSURF=101100;
dtzz=10;
ttp=200;  % tropopause temperature in Held and Suarez (1994)
AKAP=0.286;
tauta=80;
tauts=8;
sid_day=86164;
zsigb=0.7;
dttrp=2;


%% Set horizontal grid (Legendre-Gauss transformation), as in gaussmod.f90
klat=nlat;
NITER=50;   % maximum number of loop iterations
ZEPS=1e-16;  % convergence criterion
sid=zeros(1,klat);   % sin(lat)
gwd=zeros(1,klat);   % gaussian weights

z0=pi/(2*klat+1);
z1=1/(klat^2*8);
z4=2/(klat^2);

for i=1:klat/2
    z2=z0*(2*i-0.5);
    z2=cos(z2+z1/tan(z2));
    for j=1:NITER
        z3=ql(klat,z2)*qld(klat,z2);
        z2=z2-z3;
        if abs(z3)<ZEPS    % converged
            break
        end
    end
    z5=ql(klat-1,z2)/sqrt(klat-0.5);
    sid(i)=z2;
    gwd(i)=z4*(1-z2^2)/(z5^2);
    sid(klat-i+1)=-z2;
    gwd(klat-i+1)=gwd(i);
end

csq=1-sid.^2;
dla=asin(sid);  % latitudes in radian
lat=asind(sid);  % latitudes in degrees


%% Set vertical grid (after Scinocca and Haynes 1998), see PUMA preprocessor code (ppp.f90)
zsigtran=(1-ALR*dtrop/tgr)^(GA/(GASCON*ALR));
zsigmin=1-(1-zsigtran)/(nlevt);

sigmah=zeros(1,nlev);  % half levels
sigmah(1)=sigmax;   % uppermost halflevel
sigmah(nlev)=1;    % surface
sigmah(nlev-1)=zsigmin;
for i=2:(nlev-nlevt-1)   % stratosphere (linear spacing in log(sigma))
    sigmah(i)=exp((log(sigmax)-log(zsigtran))/(nlev-nlevt-1)*(nlev-nlevt-i)+log(zsigtran));
end
for i=(nlev-nlevt):(nlev-2)   % troposphere (linear spacing in sigma)
    sigmah(i)=(zsigtran-zsigmin)/(nlevt-1)*(nlev-1-i)+zsigmin;
end

sigma(1)=sigmah(1)*0.5;
sigma(2:nlev)=0.5*(sigmah(1:(nlev-1))+sigmah(2:nlev));



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create input fields
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Restoration temperature arrays  (constant and variable part)
zp0=100000;
z3=1/3;
gtc=zeros(nlon,nlat,nlev);
gtv=zeros(nlon,nlat,nlev);

for i=1:nlev
    for j=1:nlat
        zsip=sigma(i)*PSURF/zp0;
        zdtc=dtep*(sid(j).^2-z3)+dtzz*log(zsip)*csq(j);
        zdtv=zdtc-0.5*dtns*sid(j);
        gtc(:,j,i)=max([ttp (tgr-zdtc)*zsip^(AKAP)]);   % constant temperature part
        gtv(:,j,i)=max([ttp (tgr-zdtv)*zsip^(AKAP)]);
    end
end
gtv=gtv-gtc;    % variable temperature part

% This is the Held and Suarez (1994) setting for the restoration temperature field (with dtns = -20).
% Note: the sinusoidal shape of the temperature field with the dtns and dtep parameters is arbitrarily chosen, you can also define the temperature fields differently.
% You just have to make sure that the vertical temperature gradient does not become physically unstable (compared to the adiabatic lapse rate, ALR).



%% Reciprocal of damping time scales (also from Held and Suarez (1994), see ppp.f90)
rtauta_dim=1/(tauta*sid_day);
rtauts_dim=1/(tauts*sid_day);
gtdamp=zeros(nlon,nlat,nlev);

for i=1:nlev
    if sigma(i)>zsigb
        for j=1:nlat
            gtdamp(:,j,i)=rtauta_dim+(rtauts_dim-rtauta_dim)*((sigma(i)-zsigb)/(1-zsigb))*(1-sid(j)^2)^2;       % reciprocal of damping time scales
        end
    else
        gtdamp(:,:,i)=rtauta_dim;
    end
end



%% Orography (Gaussian-shaped mountain range)
gor=zeros(nlon,nlat);
% shape parameters:
Ampli=65000;
sigx=7;
sigy=20;
y0=40;
x0=60;

X=lon;
Y=lat;
[x,y]=meshgrid(X,Y);
x=x';
y=y';

oroanomaly=Ampli* exp(-((x-x0).^2/(2*sigx^2)+(y-y0).^2/(2*sigy^2)));
gor=gor+oroanomaly;   % orography



%% Surface pressure  (subroutine gridpoint, ppp.f90)
gsp=-gor./(GASCON*tgr);
zpres=0.01*PSURF*exp(gsp);  % surface pressure



%% Adjustment of the temperature field to orography
% (if the orography is nonzero, the temperature field has to be defined on pressure levels instead of sigma levels)
% The variable temperature field is not adjusted here, but it might need to be if there is for example large orography at high latitudes

hPa=sigmah*1011;
gtcoro=zeros(nlon,nlat,nlev);

for i=1:nlon
    for l=1:nlat
        hPanew=sigmah*zpres(i,l);  % pressure vector for defined sigma levels with orography
        for k=1:nlev
            % interpolation of the gtc values on sigma levels
            im=find(hPanew(k)>=hPa,1,'last');
            ip=find(hPanew(k)<=hPa,1,'first');
            if (isempty(im)==0 && im~=ip)
                gtcoro(i,l,k)=gtc(i,l,im)+((gtc(i,l,ip)-gtc(i,l,im))*(hPanew(k)-hPa(im))/(hPa(ip)-hPa(im)));
            else
                gtcoro(i,l,k)=gtc(i,l,ip);
            end
        end
    end
end

gtc=gtcoro;   % adjusted constant temperature field



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write the input fields to files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The format of the files has to be readable for PUMA - for details see the PUMA code

leer=[];
writingpath='./inputfiles/';    % change to path where you want the files to be


% Constant part of the restoration temperature field
Agtc=zeros(nlev*(1+nlat*nlon/8),8);
headgtc=[121 0 20170720 1452 nlon nlat 1 0];   % Header
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0121.sra'),leer)   % create empty file
for i=1:nlev
    headgtc(2)=i;
    Agtc((i-1)*nlat*nlon/8+i,:)=headgtc;
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0121.sra'),headgtc,'-append','delimiter',' ','precision','%9.0f')
    Agtc((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:)=reshape(gtc(:,:,i),8,nlat*nlon/8)';
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0121.sra'),Agtc((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:),'-append','delimiter',' ','precision','%9.4f')
end

% Variable part of the restoration temperature field
Agtv=zeros(nlev*(1+nlat*nlon/8),8);
headgtv=[122 0 20170720 1452 nlon nlat 1 0];   % Header
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0122.sra'),leer)
for i=1:nlev
    headgtv(2)=i;
    Agtv((i-1)*nlat*nlon/8+i,:)=headgtv;
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0122.sra'),headgtv,'-append','delimiter',' ','precision','%9.0f')
    Agtv((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:)=reshape(gtv(:,:,i),8,nlat*nlon/8)';
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0122.sra'),Agtv((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:),'-append','delimiter',' ','precision','%9.4f')
end

% Damping parameters
damp=zeros(nlev*(1+nlat*nlon/8),8);
headdamp=[123 0 20170720 1452 nlon nlat 1 0];   % Header
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0123.sra'),leer)
for i=1:nlev
    headdamp(2)=i;
    damp((i-1)*nlat*nlon/8+i,:)=headdamp;
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0123.sra'),headdamp,'-append','delimiter',' ','precision','%9.0f')
    damp((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:)=reshape(gtdamp(:,:,i),8,nlat*nlon/8)';
    dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0123.sra'),damp((i-1)*nlat*nlon/8+i+1:i*nlat*nlon/8+i,:),'-append','delimiter',' ','precision','%10.9E')
end

% Orography
headoro=[129 0 20170720 1452 nlon nlat 1 0];  % Header
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0129.sra'),headoro,'delimiter',' ','precision','%9.0f')
oro=reshape(gor,8,nlat*nlon/8)';
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0129.sra'),oro,'-append','delimiter',' ','precision','%9.3f')

% Surface pressure
headsurf=[134 0 20170720 1452 nlon nlat 1 0];   % Header
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0134.sra'),headsurf,'delimiter',' ','precision','%9.0f')
surf=reshape(zpres,8,nlat*nlon/8)';
dlmwrite(horzcat(writingpath,'N',num2str(nlat,'%03d'),'_surf_0134.sra'),surf,'-append','delimiter',' ','precision','%9.4f')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
%
% Held, I. and M. Suarez, 1994: A Proposal for the Intercomparison of the Dynamical Cores of Atmospheric General Circulation Models.
% Bulletin of the American Meteorological Society, 75, 1825-1830.
%
% Scinocca, J. and P. Haynes, 1998: Dynamical Forcing of Stratospheric Planetary Waves by Tropospheric Baroclinic Eddies.
% Journal of the Atmospheric Sciences, 55, 2361-2392.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
